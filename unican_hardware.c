/* SPDX-License-Identifier: (GPL-2.0-only OR BSD-3-Clause) */
/*
 * cansend.c - send CAN-frames via CAN_RAW sockets
 *
 * Copyright (c) 2002-2007 Volkswagen Group Electronic Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of Volkswagen nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * Alternatively, provided that this notice is retained in full, this
 * software may be distributed under the terms of the GNU General
 * Public License ("GPL") version 2, in which case the provisions of the
 * GPL apply INSTEAD OF those given above.
 *
 * The provided data structures and external interfaces from this code
 * are not restricted to be used by modules with a GPL compatible license.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 *
 * Send feedback to <linux-can@vger.kernel.org>
 *
 */

#include "unican.h"
#include "stdint.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "lib.h"
#include "stdint.h"


int s; /* can raw socket */

void can_HW_init (void)
{
    /* HW depended user code */

    char *can_name = "can0";

//    int s; /* can raw socket */

//    int mtu;
//    int enable_canfd = 1;

    struct sockaddr_can addr;

    struct ifreq ifr;

    /* open socket */
    if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        perror("socket");
        return ;
    }

    strncpy(ifr.ifr_name, can_name, IFNAMSIZ - 1);
    ifr.ifr_name[IFNAMSIZ - 1] = '\0';
    ifr.ifr_ifindex = if_nametoindex(ifr.ifr_name);
//    ifr.ifr_ifindex = 0;  // ALL_CAN


    if (!ifr.ifr_ifindex) {
        perror("if_nametoindex");
        return ;
    }

    memset(&addr, 0, sizeof(addr));
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

//    if (required_mtu > (int)CAN_MTU) {
//
//        /* check if the frame fits into the CAN netdevice */
//        if (ioctl(socket, SIOCGIFMTU, &ifr) < 0) {
//            perror("SIOCGIFMTU");
//            return 1;
//        }
//        mtu = ifr.ifr_mtu;
//
//        if (mtu != CANFD_MTU) {
//            printf("CAN interface is not CAN FD capable - sorry.\n");
//            return 1;
//        }
//
//        /* interface is ok - try to switch the socket into CAN FD mode */
//        if (setsockopt(socket, SOL_CAN_RAW, CAN_RAW_FD_FRAMES,
//                       &enable_canfd, sizeof(enable_canfd))){
//            printf("error when enabling CAN FD support\n");
//            return 1;
//        }
//    }

    /* disable default receive filter on this RAW socket */
    /* This is obsolete as we do not read from the socket at all, but for */
    /* this reason we can remove the receive list in the Kernel to save a */
    /* little (really a very little!) CPU usage.                          */
//    setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0);

    if (bind(s, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        perror("bind");
        return ;
    }

//    return s;

    /*end of user code*/
  
}

void can_HW_close()
{

  /* HW depended user code */
  close(s);
  /*end of user code*/
  
}

/*
This function could be implemented in any way, but as
result user programm should call receive_can_message()
with valid can_message structures provided.
Function name and parameters are not mandatory, but
shold be changed in unican_user.h accordingly
*/
void can_HW_receive_message ()
{
  can_message tmp_msg;
  /* HW depended user code */

    printf("can start\n");
    struct can_frame frame;

    ssize_t nbytes = read(s, &frame, sizeof(struct can_frame));

    if (nbytes < 0) {
        perror("can raw socket read");
        return;
    }

    /* paranoid check ... */
    if (nbytes < sizeof(struct can_frame)) {
        fprintf(stderr, "read: incomplete CAN frame\n");
        return;
    }

//    int ret;
//
//    ret = recv(socket, &frame, sizeof(frame), 0);
//    if (ret != sizeof(frame)) {
//        if (ret < 0)
//            perror("recv failed");
//        else
//            fprintf(stderr, "recv returned %d", ret);
//        return -1;
//    }


    for (int i = 0; i < 8; ++i) {
        printf("%X", frame.data[i]);
    }

    printf("\n");
    printf("id %d\n", frame.can_id);
    printf("len8 %d\n", frame.len8_dlc);
//    printf("len %d\n", frame.len);
    printf("dlc %d\n", frame.can_dlc);

    printf("can end\n");

    tmp_msg.can_identifier = frame.can_id;
    tmp_msg.can_dlc = frame.can_dlc;
    tmp_msg.can_extbit = frame.can_id & CAN_EFF_FLAG;
    tmp_msg.can_rtr = frame.can_id & CAN_RTR_FLAG;

    memcpy(tmp_msg.data, frame.data, sizeof(tmp_msg.data));
  /*end of user code*/
  can_receive_message (&tmp_msg);
}

/*
This function could be implemented in any way, but as
result user programm should send provided can_message
to network.
Function name and parameters are mandatory
*/
void can_HW_send_message (can_message* msg)
{
    printf("try send1\n");
  /* HW depended user code */
    int required_mtu = CAN_MTU;

    struct canfd_frame frame;

    /* parse CAN frame */
//     required_mtu = parse_canframe("001#DEADBEEF", &frame);
//
//     /* ensure discrete CAN FD length values 0..8, 12, 16, 20, 24, 32, 64 */
//     frame.len = can_fd_dlc2len(can_fd_len2dlc(frame.len));

   frame.can_id = msg->can_identifier;
   frame.can_id |= msg->can_extbit;
   frame.can_id |= msg->can_rtr;
   frame.len = msg->can_dlc;

   memcpy(frame.data, msg->data, sizeof(msg->data));
    /* ensure discrete CAN FD length values 0..8, 12, 16, 20, 24, 32, 64 */
   frame.len = can_fd_dlc2len(can_fd_len2dlc(frame.len));

    /* send frame */
    if (write(s, &frame, required_mtu) != required_mtu) {
        perror("write");
//        return -1;
    }

    printf("try send2\n");

  /*end of user code*/
}

