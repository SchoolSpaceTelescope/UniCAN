#include "unican.h"

#include "stdio.h"
//TODO:Check all this terrible english comments

/*
This event occures when valid unican message received
it's calls from within can_receive_message () so if
can_receive_message () called in interrupt handler
you will not want to do a lot of work here
*/
void unican_RX_event (uint16 msg_id, uint16 length)
{
  /* User code */

  /*end of user code*/
}

/*
This function should perform required software
reaction to unican message received from another
device.
*/
void unican_RX_message (unican_message* msg)
{
  /* User code */



  printf("RECEIVED: %u %u %u %u", msg->unican_msg_id, msg->unican_address_from, msg->unican_address_to, msg->unican_length);

  for (int i = 0; i < msg->unican_length; ++i){
      printf("[%d] %02x\n", i, msg->data[i]);
  }

    printf("\n");

  /*end of user code*/
}

/*
This function should perform required software
reaction to errors in unican
*/
void unican_error (uint16 errcode)
{
  
  /* User code */

  /*end of user code*/
}

int main(){
    printf("test can\n");

    unican_init();

    unican_message *msg = malloc(sizeof(unican_message));

    msg->data = malloc(sizeof(uint8_t) * 4);
    msg->data[0] = 0xEF;
    msg->data[1] = 0xBE;

    msg->unican_length = 2;

    msg->unican_msg_id = 1;
    msg->unican_address_from = 4;
    msg->unican_address_to = 5;

//    msg.unican_msg_id


   while(1){
       sleep(1);
       can_HW_receive_message();
        unican_take_message();
        unican_send_message(msg);
        unican_send_message(msg);
   }



}
